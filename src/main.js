var states = {
  menu: true,
  lang: false,
  play: false,
  clicked: false,
  dict: 0,
  guess: 0,
  over: false
}

var colors = {
  black: [0, 0, 0],
  white: [255, 255, 255],
  gray: [128, 128, 128],
  red: [255, 0, 0],
  fade: 1
}

var texts = {
  title: 'HUNGD',
  lang: ['EE', '||', 'HU'],
  alphabet: ['abcdefghijklmnopqrsšzžtuvwõäöüxyABCDEFGHIJKLMNOPQRSŠZŽTUVWÕÄÖÜXY', 'aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz AÁBCDEÉFGHIÍJKLMNOÓÖŐPQRSTUÚÜŰVWXYZ '],
  dic: {
    et: null,
    hu: null,
    curr: [],
    freq: []
  },
  font: null,
  len: null,
  under: '',
  clicked: null,
  show: '',
  guess: [],
  restart: ['RESTART', '||', 'MENU']
}

var board = {
  x: null,
  y: null
}

var sounds = {
  enter: null,
  click: null,
  over: null,
  error: null,
  restart: null,
  exit: null
}

function preload() {
  texts.dic.et = loadStrings('txt/et_EE.txt')
  texts.dic.hu = loadStrings('txt/hu_HU.txt')
  texts.font = loadFont('ttf/NovaMono-Regular.ttf')
  if (windowWidth >= windowHeight * 1.5) {
    board.y = windowHeight * 0.95
    board.x = board.y * 1.5
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (2 / 3)
  }
  sounds.click = loadSound('mp3/click.mp3')
  sounds.enter = loadSound('mp3/enter.mp3')
  sounds.error = loadSound('mp3/error.mp3')
  sounds.over = loadSound('mp3/over.mp3')
  sounds.restart = loadSound('mp3/restart.mp3')
  sounds.exit = loadSound('mp3/exit.mp3')
}

function setup() {
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(255)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(texts.font)
  // intro
  if (states.menu === true) {
    textAlign(CENTER, CENTER)
    textSize(board.x * 0.35)
    if (states.clicked === true) {
      colors.fade -= deltaTime / 1000
      fill(colors.black[0], colors.black[1], colors.black[2], colors.fade)
    } else {
      fill(colors.black)
    }
    noStroke()
    text(texts.title, windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.1)
  }
  // language selection
  if (states.lang === true) {
    textSize(board.x * 0.2)
    // estonian language
    if (mouseX <= windowWidth * 0.5) {
      if (states.clicked === true) {
        colors.fade -= deltaTime / 1000
        fill(colors.gray[0], colors.gray[1], colors.gray[2], colors.fade)
      } else {
        fill(colors.gray)
      }
    } else {
      if (states.clicked === true) {
        colors.fade -= deltaTime / 1000
        fill(colors.black[0], colors.black[1], colors.black[2], colors.fade)
      } else {
        fill(colors.black)
      }
    }
    noStroke()
    textAlign(LEFT, CENTER)
    text(texts.lang[0], windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.05)
    // or conditional operator
    if (states.clicked === true) {
      colors.fade -= deltaTime / 1000
      fill(colors.black[0], colors.black[1], colors.black[2], colors.fade)
    } else {
      fill(colors.black)
    }
    textAlign(CENTER, CENTER)
    text(texts.lang[1], windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.05)
    // hungarian language
    if (mouseX > windowWidth * 0.5) {
      if (states.clicked === true) {
        colors.fade -= deltaTime / 1000
        fill(colors.gray[0], colors.gray[1], colors.gray[2], colors.fade)
      } else {
        fill(colors.gray)
      }
    } else {
      if (states.clicked === true) {
        colors.fade -= deltaTime / 1000
        fill(colors.black[0], colors.black[1], colors.black[2], colors.fade)
      } else {
        fill(colors.black)
      }
    }
    textAlign(RIGHT, CENTER)
    text(texts.lang[2], windowWidth * 0.5 + board.x * 0.4, windowHeight * 0.5 - board.y * 0.05)
  }
  // play
  if (states.play === true) {
    fill(colors.black)
    noStroke()
    textAlign(LEFT, CENTER)
    textSize(board.x * 0.04)
    // rope
    noFill()
    strokeCap(SQUARE)
    stroke(colors.black)
    strokeWeight(board.x * 0.01)
    line(windowWidth * 0.5 - board.x * 0.4, -board.y * 0.1, windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3)
    if (states.guess === 0) {
      // loop
      fill(colors.black)
      noStroke()
      rect(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3, board.x * 0.025, board.y * 0.05, board.x * 0.005)
      stroke(colors.black)
      strokeWeight(board.x * 0.01)
      noFill()
      beginShape()
      curveVertex(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3)
      curveVertex(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3)
      curveVertex(windowWidth * 0.5 - board.x * 0.42, windowHeight * 0.5 - board.y * 0.2)
      curveVertex(windowWidth * 0.5 - board.x * 0.38, windowHeight * 0.5 - board.y * 0.2)
      curveVertex(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3)
      curveVertex(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 - board.y * 0.3)
      endShape()
    }
    if (states.guess >= 1) {
      // head
      fill(colors.black)
      noStroke()
      ellipse(windowWidth * 0.5 - board.x * 0.38, windowHeight * 0.5 - board.y * 0.25, board.y * 0.125, board.y * 0.125)
      fill(colors.black)
      noStroke()
      rect(windowWidth * 0.5 - board.x * 0.39, windowHeight * 0.5 - board.y * 0.22, board.x * 0.03, board.y * 0.1)
    }
    if (states.guess >= 2) {
      // body
      fill(colors.black)
      noStroke()
      rect(windowWidth * 0.5 - board.x * 0.38, windowHeight * 0.5 - board.y * 0.05, board.x * 0.1, board.y * 0.25, board.x * 0.035)
    }
    if (states.guess >= 3) {
      // left arm
      strokeCap(ROUND)
      stroke(colors.black)
      strokeWeight(board.x * 0.025)
      line(windowWidth * 0.5 - board.x * 0.418, windowHeight * 0.5 - board.y * 0.125, windowWidth * 0.5 - board.x * 0.425, windowHeight * 0.5 + board.y * 0.1)
    }
    if (states.guess >= 4) {
      // right arm
      strokeCap(ROUND)
      stroke(colors.black)
      strokeWeight(board.x * 0.025)
      line(windowWidth * 0.5 - board.x * 0.342, windowHeight * 0.5 - board.y * 0.125, windowWidth * 0.5 - board.x * 0.335, windowHeight * 0.5 + board.y * 0.1)
    }
    if (states.guess >= 5) {
      // left leg
      strokeCap(ROUND)
      stroke(colors.black)
      strokeWeight(board.x * 0.03)
      line(windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 + board.y * 0.05, windowWidth * 0.5 - board.x * 0.4, windowHeight * 0.5 + board.y * 0.3)
    }
    if (states.guess >= 6) {
      // right leg
      strokeCap(ROUND)
      stroke(colors.black)
      strokeWeight(board.x * 0.03)
      line(windowWidth * 0.5 - board.x * 0.36, windowHeight * 0.5 + board.y * 0.05, windowWidth * 0.5 - board.x * 0.36, windowHeight * 0.5 + board.y * 0.3)
    }
    if (states.guess === 7) {
      // dead
      strokeCap(SQUARE)
      stroke(colors.white)
      strokeWeight(board.x * 0.004)
      // left eye
      line(windowWidth * 0.5 - board.x * 0.39, windowHeight * 0.5 - board.y * 0.265, windowWidth * 0.5 - board.x * 0.38, windowHeight * 0.5 - board.y * 0.25)
      line(windowWidth * 0.5 - board.x * 0.39, windowHeight * 0.5 - board.y * 0.25, windowWidth * 0.5 - board.x * 0.38, windowHeight * 0.5 - board.y * 0.265)
      // right eye
      line(windowWidth * 0.5 - board.x * 0.36, windowHeight * 0.5 - board.y * 0.265, windowWidth * 0.5 - board.x * 0.35, windowHeight * 0.5 - board.y * 0.25)
      line(windowWidth * 0.5 - board.x * 0.36, windowHeight * 0.5 - board.y * 0.25, windowWidth * 0.5 - board.x * 0.35, windowHeight * 0.5 - board.y * 0.265)
      // tongue
      noFill()
      stroke(colors.red)
      beginShape()
      curveVertex(windowWidth * 0.5 - board.x * 0.3825, windowHeight * 0.5 - board.y * 0.228)
      curveVertex(windowWidth * 0.5 - board.x * 0.3825, windowHeight * 0.5 - board.y * 0.228)
      curveVertex(windowWidth * 0.5 - board.x * 0.3825, windowHeight * 0.5 - board.y * 0.215)
      curveVertex(windowWidth * 0.5 - board.x * 0.375, windowHeight * 0.5 - board.y * 0.215)
      curveVertex(windowWidth * 0.5 - board.x * 0.375, windowHeight * 0.5 - board.y * 0.227)
      curveVertex(windowWidth * 0.5 - board.x * 0.375, windowHeight * 0.5 - board.y * 0.227)
      endShape()
      // mouth
      noFill()
      stroke(colors.white)
      line(windowWidth * 0.5 - board.x * 0.385, windowHeight * 0.5 - board.y * 0.23, windowWidth * 0.5 - board.x * 0.355, windowHeight * 0.5 - board.y * 0.225)
    }
    // estonian language
    if (states.dict === 0) {
      // underscore
      fill(colors.black)
      noStroke()
      textAlign(CENTER, CENTER)
      textSize(board.x * 0.11)
      if (states.guess < 7) {
        text(texts.under, windowWidth * 0.5 + board.x * 0.1, windowHeight * 0.5 - board.y * 0.15)
      }
      if (states.over === true && mouseX > windowWidth * 0.5 + board.x * 0.1 - board.x * 0.35 && mouseX < windowWidth * 0.5 + board.x * 0.1 + board.x * 0.35 && mouseY > windowHeight * 0.5 - board.y * 0.1 - board.y * 0.05 && mouseY < windowHeight * 0.5 - board.y * 0.1 + board.y * 0.05) {
        noStroke()
        textAlign(CENTER, CENTER)
        textSize(board.x * 0.075)
        if (mouseX < windowWidth * 0.5 + board.x * 0.13) {
          fill(colors.gray)
        } else {
          fill(colors.black)
        }
        text(texts.restart[0], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025 + (-1) * board.x * 0.2, windowHeight * 0.5 - board.y * 0.15)
        fill(colors.black)
        text(texts.restart[1], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025, windowHeight * 0.5 - board.y * 0.15)
        if (mouseX > windowWidth * 0.5 + board.x * 0.13) {
          fill(colors.gray)
        } else {
          fill(colors.black)
        }
        text(texts.restart[2], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025 + board.x * 0.15, windowHeight * 0.5 - board.y * 0.15)
      } else {
        fill(colors.black)
        noStroke()
        textAlign(CENTER, CENTER)
        textSize(board.x * 0.11)
        text(texts.show, windowWidth * 0.5 + board.x * 0.1, windowHeight * 0.5 - board.y * 0.15)
      }
      // possible letters
      for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 4; j++) {
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3) {
            fill(colors.gray)
            noStroke()
            ellipse(windowWidth * 0.5 + board.x * 0.1 + (Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 3.5 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) - 1.5) * board.y * 0.1, board.x * 0.6 * 0.125)
          }
          if (texts.guess[j * 8 + i] === 1) {
            fill(colors.red)
            noStroke()
            ellipse(windowWidth * 0.5 + board.x * 0.1 + (i - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1, board.x * 0.6 * 0.125)
          }
          fill(colors.black)
          noStroke()
          textAlign(CENTER, CENTER)
          textSize(board.x * 0.05)
          text(texts.alphabet[0][i + j * 8 + 32], windowWidth * 0.5 + board.x * 0.1 + (i - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1 - board.y * 0.01)
        }
      }
    }
    // hungarian language
    if (states.dict === 1) {
      // underscore
      fill(colors.black)
      noStroke()
      textAlign(CENTER, CENTER)
      textSize(board.x * 0.11)
      if (states.guess < 7) {
        text(texts.under, windowWidth * 0.5 + board.x * 0.1, windowHeight * 0.5 - board.y * 0.15)
      }
      if (states.over === true && mouseX > windowWidth * 0.5 + board.x * 0.1 - board.x * 0.35 && mouseX < windowWidth * 0.5 + board.x * 0.1 + board.x * 0.35 && mouseY > windowHeight * 0.5 - board.y * 0.1 - board.y * 0.05 && mouseY < windowHeight * 0.5 - board.y * 0.1 + board.y * 0.05) {
        noStroke()
        textAlign(CENTER, CENTER)
        textSize(board.x * 0.075)
        if (mouseX < windowWidth * 0.5 + board.x * 0.13) {
          fill(colors.gray)
        } else {
          fill(colors.black)
        }
        text(texts.restart[0], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025 + (-1) * board.x * 0.2, windowHeight * 0.5 - board.y * 0.15)
        fill(colors.black)
        text(texts.restart[1], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025, windowHeight * 0.5 - board.y * 0.15)
        if (mouseX > windowWidth * 0.5 + board.x * 0.13) {
          fill(colors.gray)
        } else {
          fill(colors.black)
        }
        text(texts.restart[2], windowWidth * 0.5 + board.x * 0.13 + board.x * 0.025 + board.x * 0.15, windowHeight * 0.5 - board.y * 0.15)
      } else {
        fill(colors.black)
        noStroke()
        textAlign(CENTER, CENTER)
        textSize(board.x * 0.11)
        text(texts.show, windowWidth * 0.5 + board.x * 0.1, windowHeight * 0.5 - board.y * 0.15)
      }
      // possible letters
      for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 4; j++) {
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3 && (Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1))) !== 11) {
            fill(colors.gray)
            noStroke()
            ellipse(windowWidth * 0.5 + board.x * 0.1 + (Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) - 1.5) * board.y * 0.1, board.x * 0.6 * 0.125)
          }
          if (texts.guess[j * 9 + i] === 1) {
            fill(colors.red)
            noStroke()
            ellipse(windowWidth * 0.5 + board.x * 0.1 + (i - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1, board.x * 0.6 * 0.125)
          }
          fill(colors.black)
          noStroke()
          textAlign(CENTER, CENTER)
          textSize(board.x * 0.05)
          text(texts.alphabet[1][i + j * 9 + 36], windowWidth * 0.5 + board.x * 0.1 + (i - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1 - board.y * 0.01)
        }
      }
    }
  }
}

function mousePressed() {
  // intro
  if (states.menu === true) {
    states.clicked = true
    sounds.enter.play()
    sounds.click.play()
    setTimeout(function() {
      states.menu = false
      states.lang = true
      states.clicked = false
      colors.fade = 1
    }, 1000)
  }
  // language selection
  if (states.lang === true) {
    states.clicked = true
    sounds.enter.play()
    sounds.click.play()
    if (mouseX <= windowWidth * 0.5) {
      states.dict = 0
    } else {
      states.dict = 1
    }
    setTimeout(function() {
      states.lang = false
      states.play = true
      states.clicked = false
      states.fade = 1
    }, 1000)
    for (var i = 0; i < texts.alphabet[states.dict].length * 0.5; i++) {
      texts.dic.freq.push(0)
      texts.guess.push(0)
    }
    if (states.dict === 0) {
      texts.len = Math.floor(Math.random() * 3 + 3)
      for (var i = 0; i < texts.dic.et.length; i++) {
        if (texts.len === texts.dic.et[i].length) {
          texts.dic.curr.push(texts.dic.et[i].toUpperCase())
        }
      }
      for (var i = 0; i < texts.dic.curr.length; i++) {
        for (var j = 0; j < texts.alphabet[0].length; j++) {
          if (texts.dic.curr[i].includes(texts.alphabet[0][32 + j]) === true) {
            texts.dic.freq[j]++
          }
        }
      }
    } else {
      texts.len = Math.floor(Math.random() * 9 + 2)
      for (var i = 0; i < texts.dic.hu.length; i++) {
        if (texts.len === texts.dic.hu[i].length) {
          texts.dic.curr.push(texts.dic.hu[i].toUpperCase())
        }
      }
      for (var i = 0; i < texts.dic.curr.length; i++) {
        for (var j = 0; j < texts.alphabet[1].length; j++) {
          if (texts.dic.curr[i].includes(texts.alphabet[1][36 + j]) === true) {
            texts.dic.freq[j]++
          }
        }
      }
    }
    for (var i = 0; i < texts.len; i++) {
      texts.under += '_'
    }
  }
  // play
  if (states.play === true) {
    // estonian language
    if (states.dict === 0) {
      // possible letters
      for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 4; j++) {
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3 && states.guess < 7) {
            texts.clicked = texts.alphabet[0][Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 3.5 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 8 + 32]
            if (texts.guess[Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 3.5 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 8] !== 1 && states.guess < 7) {
              states.guess++
              sounds.click.play()
            } else {
              sounds.click.play()
              sounds.error.play()
            }
            texts.guess[Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 3.5 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 8] = 1
          }
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 3.5) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3 && states.guess < 7) {
            sounds.click.play()
          }
        }
      }
      texts.dic.curr = texts.dic.curr.filter(e => e.includes(texts.clicked) !== true)
      for (var i = 0; i < texts.alphabet[states.dict].length * 0.5; i++) {
        texts.dic.freq[i] = 0
      }
      for (var i = 0; i < texts.dic.curr.length; i++) {
        for (var j = 0; j < texts.alphabet[0].length; j++) {
          if (texts.dic.curr[i].includes(texts.alphabet[0][32 + j]) === true) {
            texts.dic.freq[j]++
          }
        }
      }
      if (states.guess === 7) {
        states.over = true
        texts.show = texts.dic.curr[Math.floor(Math.random() * texts.dic.curr.length)]
        sounds.over.play()
      }
      if (states.over === true && mouseX > windowWidth * 0.5 + board.x * 0.1 - board.x * 0.35 && mouseX < windowWidth * 0.5 + board.x * 0.1 + board.x * 0.35 && mouseY > windowHeight * 0.5 - board.y * 0.1 - board.y * 0.05 && mouseY < windowHeight * 0.5 - board.y * 0.1 + board.y * 0.05) {
        if (mouseX < windowWidth * 0.5 + board.x * 0.13) {
          texts.show = ''
          states.guess = 0
          states.over = false
          texts.dic.curr = []
          texts.under = ''
          texts.dic.freq = []
          texts.guess = []
          texts.len = Math.floor(Math.random() * 3 + 3)
          for (var i = 0; i < texts.dic.et.length; i++) {
            if (texts.len === texts.dic.et[i].length) {
              texts.dic.curr.push(texts.dic.et[i].toUpperCase())
            }
          }
          for (var i = 0; i < texts.dic.curr.length; i++) {
            for (var j = 0; j < texts.alphabet[0].length; j++) {
              if (texts.dic.curr[i].includes(texts.alphabet[0][32 + j]) === true) {
                texts.dic.freq[j]++
              }
            }
          }
          for (var i = 0; i < texts.len; i++) {
            texts.under += '_'
          }
          sounds.restart.play()
          sounds.click.play()
        } else {
          states.play = false
          texts.show = ''
          states.lang = true
          texts.dic.freq = []
          texts.guess = []
          states.guess = 0
          states.over = false
          texts.dic.curr = []
          texts.under = ''
          sounds.exit.play()
          sounds.click.play()
        }
      }
    }
    // hungarian language
    if (states.dict === 1) {
      // possible letters
      for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 4; j++) {
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3 && (Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1))) !== 11 && states.guess < 7) {
            texts.clicked = texts.alphabet[1][Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 9 + 36]
            if (texts.guess[Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 9] !== 1 && states.guess < 7) {
              states.guess++
              sounds.click.play()
            } else {
              sounds.click.play()
              sounds.error.play()
            }
            texts.guess[Math.floor((mouseX - (windowWidth * 0.5 + board.x * 0.1 - board.x * 4 * 0.075 - board.x * 0.0217)) / (board.x * 0.075)) + Math.floor((mouseY - (windowHeight * 0.5 + board.y * 0.25 - board.y * 1.5 * 0.1 - board.y * 0.035)) / (board.y * 0.1)) * 9] = 1
          }
          if (dist(mouseX, mouseY, windowWidth * 0.5 + board.x * 0.1 + (i - 4) * board.x * 0.075, windowHeight * 0.5 + board.y * 0.25 + (j - 1.5) * board.y * 0.1) < board.x * 0.6 * 0.125 * 0.3) {
            sounds.click.play()
          }
        }
      }
      texts.dic.curr = texts.dic.curr.filter(e => e.includes(texts.clicked) !== true)
      for (var i = 0; i < texts.alphabet[states.dict].length * 0.5; i++) {
        texts.dic.freq[i] = 0
      }
      for (var i = 0; i < texts.dic.curr.length; i++) {
        for (var j = 0; j < texts.alphabet[1].length; j++) {
          if (texts.dic.curr[i].includes(texts.alphabet[1][36 + j]) === true) {
            texts.dic.freq[j]++
          }
        }
      }
      if (states.guess === 7) {
        states.over = true
        texts.show = texts.dic.curr[Math.floor(Math.random() * texts.dic.curr.length)]
        sounds.over.play()
      }
      if (states.over === true && mouseX > windowWidth * 0.5 + board.x * 0.1 - board.x * 0.35 && mouseX < windowWidth * 0.5 + board.x * 0.1 + board.x * 0.35 && mouseY > windowHeight * 0.5 - board.y * 0.1 - board.y * 0.05 && mouseY < windowHeight * 0.5 - board.y * 0.1 + board.y * 0.05) {
        if (mouseX < windowWidth * 0.5 + board.x * 0.13) {
          texts.show = ''
          texts.dic.freq = []
          texts.guess = []
          states.guess = 0
          states.over = false
          texts.dic.curr = []
          texts.under = ''
          texts.dic.freq = []
          texts.guess = []
          texts.len = Math.floor(Math.random() * 9 + 2)
          for (var i = 0; i < texts.dic.hu.length; i++) {
            if (texts.len === texts.dic.hu[i].length) {
              texts.dic.curr.push(texts.dic.hu[i].toUpperCase())
            }
          }
          for (var i = 0; i < texts.dic.curr.length; i++) {
            for (var j = 0; j < texts.alphabet[1].length; j++) {
              if (texts.dic.curr[i].includes(texts.alphabet[1][36 + j]) === true) {
                texts.dic.freq[j]++
              }
            }
          }
          for (var i = 0; i < texts.len; i++) {
            texts.under += '_'
          }
          sounds.restart.play()
          sounds.click.play()
        } else {
          states.play = false
          states.lang = true
          texts.show = ''
          texts.dic.freq = []
          texts.guess = []
          states.guess = 0
          states.over = false
          texts.dic.curr = []
          texts.under = ''
          sounds.exit.play()
          sounds.click.play()
        }
      }
    }
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
  if (windowWidth >= windowHeight * 1.5) {
    board.y = windowHeight * 0.95
    board.x = board.y * 1.5
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (2 / 3)
  }
}
